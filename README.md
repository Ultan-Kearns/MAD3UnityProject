# Fourth Year Mobile Application Development Project - UnityProject
## Developed By Ultan Kearns G00343745

## Important notes
 + Development Diary is supplemented by issues on github
 + Game works with no errors 
 + Video of gameplay on youtube: https://www.youtube.com/watch?v=6pfmrMHwq9U

## Game Concept
This is a 2d platformer game programmed in Unity. The object of the game is to survive a number of waves while dodging bullets and trying not to fall off the platforms or get hit by the bullets. The player will try and attain a high score which will increase as the game progresses and the player will be encouraged to get a high score.

## Implementation
The Implementation of the project will be discussed in the Developer Diary, which I will link to here: <a href ="https://github.com/Ultan-Kearns/MAD3UnityProject/tree/master/DeveloperDiary">Link to developer diary</a>.  In the developer diary I discuss the implementation, design, and problems I faced while creating the game. 

## About Unity
<img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.techhive.com%2Fimages%2Farticle%2F2015%2F03%2Funity-logo-100571261-large.png&f=1&nofb=1" alt = "Unity logo" width="500px" height ="250px"/>
Unity is a game engine which can be used to create 2D or 3D games, for my purposes only the 2D is relevant.  The engine is fairly easy to use and all relevant info can be found on the Unity Docs.

## Game Features
+ Difficulty System
+ Audio along with options to mute
+ 3 Scenes Game, Gameover and Main menu
+ Features 8 bit classical music for a retro feel
+ IT HAS A PIGEON!!!!!!!!
+ Offers the player a difficult yet fair challenge

## Platforms 

+ Windows
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.jyn6lj87cqkTl_dIfXTRRQHaGp%26pid%3DApi&f=1" alt = "Windows logo" width="500px" height ="250px"/>

+ Linux
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.hYr3PjRLGAm7nluRO3PCcwHaIL%26pid%3DApi&f=1" alt = "Linux logo" width="500px" height ="250px"/>

+ Mac OS
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fen%2Fthumb%2Fb%2Fb9%2FMacOS_original_logo.svg%2F1200px-MacOS_original_logo.svg.png&f=1&nofb=1" alt = "Mac OS logo" width="500px" height ="250px"/>

### Notes
All music is referenced in the Developer Diary as it was not made by me, all assets were made by the designer of the game.
